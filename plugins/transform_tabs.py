#!/usr/bin/env python3

import re
import sys
import json
from io import StringIO
from functools import partial

outer_re = re.compile(r'{{\s*#tabs\s*}}\n(.*?){{\s*#endtabs\s*}}', re.DOTALL)
inner_re = re.compile(r'{{\s*#tab\s+([^}]+)}}\n(.*?)(?={{\s*#tab|\Z)', re.DOTALL)

def transform_tabs(core, count):
    outfile = StringIO()
    p = partial(print, file=outfile)
    
    it = inner_re.findall(core)
    p('<div class="tab">')
    for i, result in enumerate(it):
        header, _ = result
        p('''  <button class="tablinks" id='tabs_button_{count}_{i}' onclick="openCity(event, 'tabs_hash_{count}_{i}')">{header}</button>'''.format(
            count=count, header=header, i=i))
    p('</div>')
    p()

    for i, result in enumerate(it):
        _, contents = result
        p('<div id="tabs_hash_{count}_{i}" class="tabcontent">'.format(count=count, i=i))
        p()
        p(contents)
        p()
        p('</div>')
    
    # "Click" the first tab to make it visible
    p('''
<script type="text/javascript">document.addEventListener("DOMContentLoaded", function(){{
document.getElementById("tabs_button_{count}_0").click();
}});</script>
    '''.format(count=count))

    return outfile.getvalue()


def transform_page(current_md):
    result = outer_re.search(current_md)
    count = 0

    # Python 3.8 syntax
    # while result := inner_re.search(core):
    while result:
        current_md = result.string[:result.start()] + transform_tabs(result.group(1), count) + result.string[result.end():]
        result = outer_re.search(current_md)
        count += 1

    return current_md

if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == 'supports':
            # sys.argv[2] is the renderer name
            sys.exit(0)
    
    inp = sys.stdin
    context, book = json.load(inp)
    if 'sections' in book:
        for item in book['sections']:
            item['Chapter']['content'] = transform_page(item['Chapter']['content'])
  
    print(json.dumps(book), end='')
