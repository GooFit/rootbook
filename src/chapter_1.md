# IO

Let's take a look at loading a file. Assume we have the following tree:

```
- MyTree (100 items)
    - x (a double)
    - y (a double)
```

Writing will be added later.


Here's how we would read it and loop over the contents:

{{#tabs}}

{{#tab Classic ROOT}}
```cpp
TFile f0("file0.root");
TTree* my_tree = (TTree*) f0.Get("MyTree");
f0.Close();
```

Note that we could also use `TFile::Open` and get a pointer. Or we could make this a pointer, etc.
Calling close is even more important if you do that.

{{#tab Modern ROOT}}
```cpp
TTreeReader my_tree_reader;
```

{{#tab DataFrame ROOT}}

```cpp

```

{{#tab Python}}
```python
def f(x):
    return y
```

{{#endtabs}}


